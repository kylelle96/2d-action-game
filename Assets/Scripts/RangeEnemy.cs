using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangeEnemy : Enemy
{
    [SerializeField] float stopDistance;
    [SerializeField] Transform firePoint;
    [SerializeField] GameObject bullet;

    float attackTime;

    Animator anim;

    public override void Start()
    {
        base.Start();
        anim = GetComponent<Animator>();
    }


    private void Update()
    {
        if (player != null)
        {
            var distance = Vector2.Distance(transform.position, player.position);
            if(distance > stopDistance)
            {
                transform.position = Vector2.MoveTowards(transform.position, player.position, speed * Time.deltaTime);
            }

            if(Time.time > attackTime)
            {
                attackTime = Time.time + timeBetweenAttacks;
                anim.SetTrigger("attack");
            }
        }
    }

    public void RangeAttack()
    {
        Vector2 direction = player.position - firePoint.position;
        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        Quaternion rotation = Quaternion.AngleAxis(angle - 90, Vector3.forward);
        firePoint.rotation = rotation;

        Instantiate(bullet, firePoint.position, firePoint.rotation);
    }
}
