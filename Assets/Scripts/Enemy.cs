using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public int health;
    public int damage = 1;
    public float speed;
    public float timeBetweenAttacks = 2f;
    public float pickupChance;
    public GameObject[] pickups;

    
    protected Transform player;

    public virtual void Start()
    {
        player = FindObjectOfType<Player>().transform; 
    }

    public void TakeDamage(int damage)
    {
        health -= damage;
        if(health <= 0)
        {
            float randomChance = Random.Range(0, 1.01f);
            if(randomChance < pickupChance)
            {
                GameObject randomPickup = pickups[Random.Range(0, pickups.Length)];
                Instantiate(randomPickup, transform.position, transform.rotation);
            }

            Destroy(gameObject);
        }
    }
}
