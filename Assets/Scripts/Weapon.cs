using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{
    [SerializeField] Transform _fireballPrefab;
    [SerializeField] Transform _shotPoint;
    [SerializeField] float _timeBetweenShot = 0.5f;
    [SerializeField] bool isAuto;

    private float _shotTime = 0;

    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Vector2 direction = Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position;
        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        Quaternion rotation = Quaternion.AngleAxis(angle - 90, Vector3.forward);
        transform.rotation = rotation;

        if (!isAuto)
        {
            if (Input.GetButtonDown("Fire1"))
            {
                if (Time.time >= _shotTime)
                {
                    Instantiate(_fireballPrefab, _shotPoint.position, transform.rotation);
                    _shotTime = Time.time + _timeBetweenShot;
                }
            }
        }
        else
        {
            if (Input.GetButton("Fire1"))
            {
                if (Time.time >= _shotTime)
                {
                    Instantiate(_fireballPrefab, _shotPoint.position, transform.rotation);
                    _shotTime = Time.time + _timeBetweenShot;
                }
            }
        }
       
    }
}
