using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    [SerializeField] int damage = 1;
    [SerializeField] float _speed;
    [SerializeField] float _lifeTime;
    [SerializeField] GameObject _particle;
    [SerializeField] bool isExplosive;
    [SerializeField] LayerMask layerMaskExplosive;

    // Start is called before the first frame update
    void Start()
    {
        Invoke("DestroyProjectile", _lifeTime);
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector2.up * _speed * Time.deltaTime);
    }

    private void DestroyProjectile()
    {
        Instantiate(_particle, transform.position, Quaternion.identity);
      
        Destroy(gameObject);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.GetComponent<Enemy>())
        {
            var enemy = collision.GetComponent<Enemy>();
            if(!isExplosive)
                enemy.TakeDamage(damage);
            else
            {
                var enemiesDetected = Physics2D.OverlapCircleAll(this.gameObject.transform.position, 1.5f, layerMaskExplosive);
                foreach (var enemyInRadius in enemiesDetected)
                {
                    enemyInRadius.gameObject.GetComponent<Enemy>().TakeDamage(damage);
                }

            }

            DestroyProjectile();
        }
    }
}
