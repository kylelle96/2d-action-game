using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    [SerializeField] float speed = 5f;
    [SerializeField] int health = 3;
    [SerializeField] Image[] heartContainers;


    //[SerializeField] Sprite fullHeart;
    //[SerializeField] Sprite emptyHeart;
    [SerializeField] Color color;


    private Rigidbody2D rb;
    private Animator anim;

    private Vector2 moveAmount; 

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        UpdateHealthUI(health);
    }

    // Update is called once per frame
    void Update()
    {
        var horizontal = Input.GetAxisRaw("Horizontal");
        var vertical = Input.GetAxisRaw("Vertical");

        Vector2 moveInput = new Vector2(horizontal, vertical);

        moveAmount = moveInput.normalized * speed;

        if(moveInput != Vector2.zero)
        {
            anim.SetBool("isRunning", true);
        }
        else
        {
            anim.SetBool("isRunning", false);
        }
        

    }

    private void FixedUpdate()
    {
        rb.MovePosition(rb.position + moveAmount * Time.deltaTime);
    }


    public void TakeDamage(int damage)
    {
        health -= damage;
        UpdateHealthUI(health);
        if (health <= 0)
        {
            Destroy(gameObject);
        }
    }

    public void ChangeWeapon(Weapon weaponToEquip)
    {
        Destroy(GameObject.FindGameObjectWithTag("Weapon"));
        Instantiate(weaponToEquip, transform.position, transform.rotation, transform);
    }

    void UpdateHealthUI(int currentHealth)
    {
        for (int i = 0; i < heartContainers.Length; i++)
        {
            if (i < currentHealth)
                heartContainers[i].color = color;
            else
                heartContainers[i].color = Color.black;
        }
    }

    public void Heal(int healAmount)
    {
        if(health + healAmount > 5)
            health = 5;
        else
            health += healAmount;

        UpdateHealthUI(health);
    }
}
